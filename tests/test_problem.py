import os.path
import pickle
import unittest

from d3m.metadata import problem


class TestProblem(unittest.TestCase):
    def test_basic(self):
        self.maxDiff = None

        problem_doc_path = os.path.join(os.path.dirname(__file__), 'data', 'problems', 'iris_problem_1', 'problemDoc.json')

        self.assertEqual(problem.parse_problem_description(problem_doc_path), {
            'id': 'iris_problem_1',
            'digest': '8d6ae5f7a2e1a2bd15899e2a9884df8ff3333d4333227385a71011ca812a41ac',
            'version': '1.0',
            'name': 'Distinguish Iris flowers',
            'description': 'Distinguish Iris flowers of three related species.',
            'schema': problem.PROBLEM_SCHEMA_VERSION,
            'problem': {
                'task_type': problem.TaskType.CLASSIFICATION,
                'task_subtype': problem.TaskSubtype.MULTICLASS,
                'performance_metrics': [
                    {
                        'metric': problem.PerformanceMetric.ACCURACY,
                        'params': {},
                    }
                ]
            },
            'inputs': [
                {
                    'dataset_id': 'iris_dataset_1',
                    'targets': [
                        {
                            'target_index': 0,
                            'resource_id': 'learningData',
                            'column_index': 5,
                            'column_name': 'species',
                        }
                    ]
                }
            ],
            'outputs': {
                'predictions_file': 'predictions.csv',
                'scores_file': 'scores.csv'
            }
        })

    def test_unparse(self):
        self.assertEqual(problem.TaskType.CLASSIFICATION.unparse(), 'classification')
        self.assertEqual(problem.TaskSubtype.MULTICLASS.unparse(), 'multiClass')
        self.assertEqual(problem.PerformanceMetric.ACCURACY.unparse(), 'accuracy')

    def test_normalize(self):
        self.assertEqual(problem.PerformanceMetric._normalize(0, 1, 0.5), 0.5)
        self.assertEqual(problem.PerformanceMetric._normalize(0, 2, 0.5), 0.25)
        self.assertEqual(problem.PerformanceMetric._normalize(1, 2, 1.5), 0.5)

        self.assertEqual(problem.PerformanceMetric._normalize(-1, 0, -0.5), 0.5)
        self.assertEqual(problem.PerformanceMetric._normalize(-2, 0, -1.5), 0.25)
        self.assertEqual(problem.PerformanceMetric._normalize(-2, -1, -1.5), 0.5)

        self.assertEqual(problem.PerformanceMetric._normalize(1, 0, 0.5), 0.5)
        self.assertEqual(problem.PerformanceMetric._normalize(2, 0, 0.5), 0.75)
        self.assertEqual(problem.PerformanceMetric._normalize(2, 1, 1.5), 0.5)

        self.assertEqual(problem.PerformanceMetric._normalize(0, -1, -0.5), 0.5)
        self.assertEqual(problem.PerformanceMetric._normalize(0, -2, -1.5), 0.75)
        self.assertEqual(problem.PerformanceMetric._normalize(-1, -2, -1.5), 0.5)

        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('inf'), 0, 0.0), 1.0)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('inf'), 0, 0.5), 0.9997500000052083)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('inf'), 0, 1000.0), 0.5378828427399902)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('inf'), 0, 5000.0), 0.013385701848569713)

        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('inf'), 1, 1.0), 1.0)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('inf'), 1, 1.5), 0.9997500000052083)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('inf'), 1, 1000.0), 0.5382761574524354)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('inf'), 1, 5000.0), 0.013399004523107192)

        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('inf'), -1, -1.0), 1.0)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('inf'), -1, -0.5), 0.9997500000052083)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('inf'), -1, 1000.0), 0.5374897097430198)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('inf'), -1, 5000.0), 0.01337241229216877)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('inf'), -1, 0.0), 0.9995000000416667)

        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('-inf'), 0, 0.0), 1.0)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('-inf'), 0, -0.5), 0.9997500000052083)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('-inf'), 0, -1000.0), 0.5378828427399902)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('-inf'), 0, -5000.0), 0.013385701848569713)

        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('-inf'), 1, 1.0), 1.0)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('-inf'), 1, 0.5), 0.9997500000052083)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('-inf'), 1, -1000.0), 0.5374897097430198)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('-inf'), 1, -5000.0), 0.01337241229216877)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('-inf'), 1, 0.0), 0.9995000000416667)

        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('-inf'), -1, -1.0), 1.0)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('-inf'), -1, -1.5), 0.9997500000052083)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('-inf'), -1, -1000.0), 0.5382761574524354)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(float('-inf'), -1, -5000.0), 0.013399004523107192)

        self.assertAlmostEqual(problem.PerformanceMetric._normalize(0, float('inf'), 0.0), 1 - 1.0)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(0, float('inf'), 0.5), 1 - 0.9997500000052083)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(0, float('inf'), 1000.0), 1 - 0.5378828427399902)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(0, float('inf'), 5000.0), 1 - 0.013385701848569713)

        self.assertAlmostEqual(problem.PerformanceMetric._normalize(1, float('inf'), 1.0), 1 - 1.0)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(1, float('inf'), 1.5), 1 - 0.9997500000052083)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(1, float('inf'), 1000.0), 1 - 0.5382761574524354)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(1, float('inf'), 5000.0), 1 - 0.013399004523107192)

        self.assertAlmostEqual(problem.PerformanceMetric._normalize(-1, float('inf'), -1.0), 1 - 1.0)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(-1, float('inf'), -0.5), 1 - 0.9997500000052083)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(-1, float('inf'), 1000.0), 1 - 0.5374897097430198)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(-1, float('inf'), 5000.0), 1 - 0.01337241229216877)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(-1, float('inf'), 0.0), 1 - 0.9995000000416667)

        self.assertAlmostEqual(problem.PerformanceMetric._normalize(0, float('-inf'), 0.0), 1 - 1.0)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(0, float('-inf'), -0.5), 1 - 0.9997500000052083)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(0, float('-inf'), -1000.0), 1 - 0.5378828427399902)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(0, float('-inf'), -5000.0), 1 - 0.013385701848569713)

        self.assertAlmostEqual(problem.PerformanceMetric._normalize(1, float('-inf'), 1.0), 1 - 1.0)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(1, float('-inf'), 0.5), 1 - 0.9997500000052083)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(1, float('-inf'), -1000.0), 1 - 0.5374897097430198)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(1, float('-inf'), -5000.0), 1 - 0.01337241229216877)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(1, float('-inf'), 0.0), 1 - 0.9995000000416667)

        self.assertAlmostEqual(problem.PerformanceMetric._normalize(-1, float('-inf'), -1.0), 1 - 1.0)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(-1, float('-inf'), -1.5), 1 - 0.9997500000052083)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(-1, float('-inf'), -1000.0), 1 - 0.5382761574524354)
        self.assertAlmostEqual(problem.PerformanceMetric._normalize(-1, float('-inf'), -5000.0), 1 - 0.013399004523107192)

    def test_pickle(self):
        value = problem.PerformanceMetric.ACCURACY

        pickled = pickle.dumps(value)
        unpickled = pickle.loads(pickled)

        self.assertEqual(value, unpickled)
        self.assertIs(value.get_function(), unpickled.get_function())


if __name__ == '__main__':
    unittest.main()
